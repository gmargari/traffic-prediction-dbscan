#include "dbscan.h"

#include <assert.h>
#include <opencv2/opencv.hpp>

// Implementation based on pseudocode of https://en.wikipedia.org/wiki/DBSCAN

//=============================================================================
// DBSCAN ()
//=============================================================================
DBSCAN::DBSCAN(IntFlowMap* data_flows, double eps, size_t min_pts)
    : data_flows_(data_flows), eps_(eps), min_pts_(min_pts) {
    labels_.resize(data_flows->size());
    clustered_.resize(data_flows->size());
    for (IntFlowMap::iterator it = data_flows_->begin(); it != data_flows_->end(); ++it) {
        Point p(it->first, it->second);
        assert(p.id < labels_.size());
        labels_[p.id] = not_visited;
        clustered_[p.id] = false;
        points_.insert(p);
    }
}

//=============================================================================
// run ()
//=============================================================================
void DBSCAN::run() {
    PointSet neighbors;

    // For each point P in dataset D
    for (PointSet::iterator it = points_.begin(); it != points_.end(); ++it) {
        Point p = *it;
        // If P is visited
        if (labels_[p.id] == visited) {
            // Continue next point
            continue;
        }
        // Mark P as visited
        labels_[p.id] = visited;
        // NeighborPts = regionQuery(P, eps)
        neighbors = getNeighbors(p);
        // If sizeof(NeighborPts) < MinPts
        if (neighbors.size() < min_pts_) {
            // Mark P as NOISE
            labels_[p.id] = noise;  // NOTE: may still be included in a cluster
        } else {
            // C = next cluster
            // expandCluster(P, NeighborPts, C, eps, MinPts)
            PointSet cluster;
            expandCluster(p, neighbors, cluster);
            clusters_.push_back(cluster);
        }
    }
}

//=============================================================================
// expandCluster ()
//=============================================================================
void DBSCAN::expandCluster(Point p, PointSet& neighbors, PointSet& cluster) {
    // TODO: check p not already in cluster
    PointSet neighbors2;

    // Add P to cluster C
    cluster.insert(p);
    // For each point P2 in NeighborPts
    while (neighbors.size() > 0) {
        PointSet::iterator it = neighbors.begin();
        Point p2 = *it;
        neighbors.erase(it);
        // If P2 is not visited
        if (labels_[p2.id] == not_visited) {
            // Mark P2 as visited
            labels_[p2.id] = visited;
            // NeighborPts2 = regionQuery(P2, eps)
            neighbors2 = getNeighbors(p2);
            // If sizeof(NeighborPts2) >= MinPts
            if (neighbors2.size() >= min_pts_) {
                // NeighborPts = NeighborPts joined with NeighborPts'
                neighbors.insert(neighbors2.begin(), neighbors2.end());
            }
        }
        // If P2 is not yet member of any cluster
        if (clustered_[p2.id] == false) {
            // Add P2 to cluster C
            cluster.insert(p2);
            clustered_[p2.id] = true;
        }
    }
    // This can actually happen: http://stackoverflow.com/a/21994903
    //assert(cluster.size() >= min_pts_);
}

//=============================================================================
// getNeighbors ()
//=============================================================================
PointSet DBSCAN::getNeighbors(Point p) {
    PointSet neighbors;
    // Return all points within P's eps-neighborhood (including P)
    for (PointSet::iterator it = points_.begin(); it != points_.end(); ++it) {
        Point p2 = *it;
        if (distance(p, p2) < eps_) {
            neighbors.insert(p2);
        }
    }
    return neighbors;
}

//=============================================================================
// distance ()
//=============================================================================
double DBSCAN::distance(Point p1, Point p2) {
    // TODO: precalculate distance matrix
    double sum = 0;
    assert(p1.vec.size() == p2.vec.size());
    for (size_t i = 0; i < p1.vec.size(); i++) {
        sum += pow(p1.vec[i] - p2.vec[i], 2);
    }
    return sqrt(sum);
}

//=============================================================================
// printClusters ()
//=============================================================================
void DBSCAN::printClusters() {
    std::cout << "Clusters: " << clusters_.size() << std::endl;
    for (std::vector<PointSet>::iterator cit = clusters_.begin(); cit != clusters_.end(); ++cit) {
        PointSet cluster = *cit;
        for (PointSet::iterator pit = cluster.begin(); pit != cluster.end(); ++pit) {
            Point p = *pit;
            std::cout << p.id << " ";
        }
        std::cout << std::endl;
    }
}
