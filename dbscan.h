#ifndef DBSCAN_H_
#define DBSCAN_H_

#include <cstring>  // for size_t
#include "data_types.h"

typedef enum Label {visited, not_visited, noise} Label;

//=============================================================================
// DBSCAN class
//=============================================================================
class DBSCAN {
  public:
    DBSCAN(IntFlowMap* data_flows, double eps, size_t min_pts);
    void run();
    void printClusters();

  private:
    void expandCluster(Point p, PointSet& neighbors, PointSet& cluster);
    PointSet getNeighbors(Point p);
    double distance(Point p1, Point p2);

    IntFlowMap* data_flows_;
    PointSet points_;
    double eps_;
    size_t min_pts_;
    std::vector<PointSet> clusters_;
    std::vector<Label> labels_;
    std::vector<bool> clustered_;
};

#endif  // DBSCAN_H_
