#include <vector>
#include <map>
#include <set>

#ifndef DATA_TYPES_H_
#define DATA_TYPES_H_

typedef std::vector<double> DoubleVector;
typedef DoubleVector Flow;
typedef std::map<int, Flow> IntFlowMap;

// for DBSCAN
class Point {
  public:
    Point(int pid, DoubleVector pvec) : id(pid), vec(pvec) {}
    const bool operator < (const Point &other) const {
        return (other.id < id);
    }
    size_t id;
    DoubleVector vec;
};
typedef std::set<Point> PointSet;

#endif  // DATA_TYPES_H_
