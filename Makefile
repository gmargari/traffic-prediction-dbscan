CC = g++
CPPFLAGS = -Wall
SRCS = main.cpp dbscan.cpp
LIBS = -lopencv_core

# Uncomment exactly one
OPT = -O2 -DNDEBUG      # (A) Production use (optimized mode)
#OPT = -g2               # (B) Debug mode, w/ full line-level debugging symbols
#OPT = -O2 -g2 -DNDEBUG  # (C) Profiling mode: opt, but w/debugging symbols

# define the executable name
MAIN = run

OBJS = $(SRCS:.cpp=.o)

all:    $(MAIN)

$(MAIN): $(OBJS)
	$(CC) $(CPPFLAGS) $(OPT) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

.cpp.o:
	$(CC) $(CPPFLAGS) $(OPT) -c $< -o $@

clean:
	$(RM) *.o *~ $(MAIN)
