#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <numeric>
#include <unistd.h>

#include "data_types.h"
#include "dbscan.h"

//=============================================================================
// parseFile ()
//=============================================================================
IntFlowMap parseFile(char* filename) {
    IntFlowMap flows;
    std::ifstream infile(filename);
    std::string line;
    int id, day_of_week;
    double num;

    while (std::getline(infile, line)) {
      std::istringstream iss(line);
      DoubleVector flow;
      iss >> id;
      iss >> day_of_week;
      while (iss >> num) {
        flow.push_back(num);
      }
      flows[id] = flow;
    }

    return flows;
}

//=============================================================================
// printFlow ()
//=============================================================================
void printFlow(Flow flow) {
    for (Flow::iterator it = flow.begin(); it != flow.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
}

//=============================================================================
// printFlows ()
//=============================================================================
void printFlows(IntFlowMap flows) {
    for (IntFlowMap::iterator it = flows.begin(); it != flows.end(); ++it) {
        std::cout << it->first << ": ";
        printFlow(it->second);
    }
}

//=============================================================================
// createFeatureVector ()
//=============================================================================
void createFeatureVector(DoubleVector& values){
    DoubleVector valuesDiffs;
    for(unsigned int j = 1 ; j < values.size() ; j++){
        valuesDiffs.push_back((double)fabs(values[j] - values[j-1]));
    }
    double minFlow = *std::min_element(values.begin(),values.end());
    double maxFlow = *std::max_element(values.begin(),values.end());
    double meanFlow = std::accumulate(values.begin(),values.end(),0.0)/values.size();
    double meanChangeOfFlow = std::accumulate(valuesDiffs.begin(),valuesDiffs.end(),0.0)/valuesDiffs.size();
    values.clear();
    values.push_back(minFlow);
    values.push_back(maxFlow);
    values.push_back(meanFlow);
    values.push_back(meanChangeOfFlow);
}

//=============================================================================
// replaceFlowsWithFeatures ()
//=============================================================================
void replaceFlowsWithFeatures(IntFlowMap& flows) {
    for (IntFlowMap::iterator it = flows.begin(); it != flows.end(); ++it) {
        createFeatureVector(it->second);
    }
}

//=============================================================================
// normalizeData ()
//=============================================================================
void normalizeData(IntFlowMap& flows) {
    // Find max value per column
    DoubleVector maxs;
    for (IntFlowMap::iterator it = flows.begin(); it != flows.end(); ++it) {
        DoubleVector& values = it->second;
        if (maxs.size() == 0) {
            maxs = values;
        }
        for (size_t i = 0; i < values.size(); i++) {
            maxs[i] = std::max(maxs[i], values[i]);
        }
    }
    // Divide each value with its column's max
    for (IntFlowMap::iterator it = flows.begin(); it != flows.end(); ++it) {
        DoubleVector& values = it->second;
        for (size_t i = 0; i < values.size(); i++) {
            values[i] /= maxs[i];
        }
    }
}

//=============================================================================
// main ()
//=============================================================================
int main(int argc, char** argv) {
    if (argc != 4) {
        std::cerr << "Syntax: " << argv[0] << " <filename> <eps> <min_pts>" << std::endl;
        exit(1);
    } else if (access(argv[1], F_OK ) == -1) {
        std::cerr << "File '" << argv[1] << "' does not exist" << std::endl;
        exit(1);
    }

    char *filename = argv[1];
    double eps = atof(argv[2]);
    size_t min_pts = atoi(argv[3]);

    IntFlowMap flows = parseFile(filename);
    replaceFlowsWithFeatures(flows);
    normalizeData(flows);
    // TODO: standarize/normalize data
    DBSCAN dbscan(&flows, eps, min_pts);
    dbscan.run();
    dbscan.printClusters();
}
